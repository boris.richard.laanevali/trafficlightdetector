#!/usr/bin/env python

import rospy
import numpy as np
import message_filters
from std_msgs.msg import Bool
from std_msgs.msg import Float32
from geometry_msgs.msg import Vector3

pub_length=rospy.Publisher('traffic_light_length',Float32,queue_size=10)

def traffic_light_analysis():
    rospy.init_node('traffic_light_analysis')
    s1=rospy.Subscriber('traffic_light_detected',Bool,traffic_light_detected)
    s2=rospy.Subscriber('traffic_light_size', Vector3,traffic_light_size    )
    sub_detect=message_filters.Subscriber('traffic_light_detected',Bool,queue_size=10)
    sub_size  =message_filters.Subscriber('traffic_light_size', Vector3,queue_size=10)
    ts= message_filters.ApproximateTimeSynchronizer([sub_detect,sub_size],10,0.1,allow_headerless=True)
    ts.registerCallback(traffic_light_length)
    rospy.spin()

def traffic_light_length(detected,size):
    if detected:                      #FIXME: better to check size.x>0 and size.y>0:
        rospy.loginfo('traffic_light_length %s, size %s',detected,size)
        pub_length.publish(size.y/3)  #FIXME: deal with horizontal TL

def traffic_light_detected(detected):
    rospy.loginfo('traffic_light_detected %s',detected) #do nothing, subscription is required by the task

def traffic_light_size(size):
    rospy.loginfo('traffic_light_size: %s',size) #do nothing, subscription is required by the task

if __name__ == '__main__':
    try:
        traffic_light_analysis()
    except rospy.ROSInterruptException:
        pass
