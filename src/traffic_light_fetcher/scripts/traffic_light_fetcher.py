#!/usr/bin/env python

import cv2
import rospy
import numpy as np
import tensorflow as tf
from threading import RLock
from threading import Thread
from std_msgs.msg import Bool
from std_msgs.msg import Float32
from geometry_msgs.msg import Vector3

DEBUG=False
TL_SCORE=50
LABEL_TRAFFIC_LIGHT=10

color=(128,0,0)
filename='test.mp4'
model_name='ssd_resnet50_v1_fpn_640x640_coco17_tpu-8'
url='http://download.tensorflow.org/models/object_detection/tf2/20200711/'+model_name+'.tar.gz'

lock=RLock()
detected=False
vector=Vector3(0,0,0)

pub_detect=rospy.Publisher('traffic_light_detected',Bool,queue_size=10)
pub_size  =rospy.Publisher('traffic_light_size', Vector3,queue_size=10)

def traffic_light_pub():
    rate=rospy.Rate(1)  #publish every second
    while not rospy.is_shutdown():
        with lock:
            d=detected
            v=vector
        if d:pub_detect.publish(d)
        pub_size.publish(v)
        rate.sleep()

def traffic_light_fetcher():
    global vector,detected
    rospy.init_node('traffic_light_fetcher',anonymous=True)
    Thread(target=traffic_light_pub).start()
    model_dir=tf.keras.utils.get_file(fname=model_name,untar=True,origin=url)
    model=tf.saved_model.load(str(model_dir)+"/saved_model")
    cap=cv2.VideoCapture(filename)
    fps=cap.get(cv2.CAP_PROP_FPS)
    skip_rate=int(fps)  #/4 for bad hw
    frame_num=0
    while cap.isOpened():
        success,frame=cap.read()
        if success:
            if frame_num % skip_rate == 0:
                v=traffic_light_detect(model,frame,frame_num)
                with lock:
                    if v is None:
                        detected=False
                        vector=Vector3(0,0,0)
                    else:
                        detected=True
                        vector=v
            frame_num+=1
        else: break
    cap.release()
    rospy.signal_shutdown("done")

def traffic_light_detect(model,frame,num):
    frame_rgb=cv2.cvtColor(frame,cv2.COLOR_BGR2RGB)
    input_tensor=tf.convert_to_tensor(frame_rgb)
    input_tensor=input_tensor[tf.newaxis, ...]
    output=model(input_tensor)
    num_detections=int(output.pop('num_detections'))
    output={key:value[0,:num_detections].numpy() for key,value in output.items()}
    output['num_detections']=num_detections
    output['detection_classes']=output['detection_classes'].astype(np.int64)
    output['boxes']=[{
        "y":int(box[0]*frame_rgb.shape[0]),"x":int(box[1]*frame_rgb.shape[1]),"y2":int(box[2]*frame_rgb.shape[0]),"x2":int(box[3]*frame_rgb.shape[1])
      }for box in output['detection_boxes']]
    for idx in range(len(output['boxes'])):
      obj_class=output["detection_classes"][idx]
      score=int(output["detection_scores"][idx]*100)
      box=output["boxes"][idx]
      if obj_class==LABEL_TRAFFIC_LIGHT and score>TL_SCORE:
         v=Vector3(x=box["x2"]-box["x"],y=box["y2"]-box["y"],z=score) #using z as score
         if DEBUG:
            print("tl found in frame:",num,":",v)
            cv2.rectangle(frame_rgb,(box["x"],box["y"]),(box["x2"],box["y2"]),color,2)
            output_frame=cv2.cvtColor(frame_rgb,cv2.COLOR_RGB2BGR)
            cv2.imwrite(str(num)+".bmp",output_frame)
         return v #single tl is ok

if __name__ == '__main__':
    try:
        traffic_light_fetcher()
    except rospy.ROSInterruptException:
        pass
