#!/usr/bin/env python3

import rospy
import tensorflow as tf
from tensorflow import keras
from std_msgs.msg import Bool
from std_msgs.msg import String
from std_msgs.msg import Float32
from geometry_msgs.msg import Vector3

# don't wanna use class
pub_detect = rospy.Publisher('traffic_light_detected',Bool,   queue_size=10)
pub_size   = rospy.Publisher('traffic_light_size',    Vector3,queue_size=10)

def traffic_light_fetcher():
    rospy.init_node('traffic_light_fetcher', anonymous=True)

    name='ssd_resnet50_v1_fpn_640x640_coco17_tpu-8'
    url='http://download.tensorflow.org/models/object_detection/tf2/20200711/'+model_name+'.tar.gz'
    model_dir = tf.keras.utils.get_file(fname=model_name, untar=True, origin=url)
    print("Model path:",str(model_dir))
    model = tf.saved_model.load(str(model_dir) + "/saved_model")

    img_bgr = cv2.imread(file_name)
    img_rgb = cv2.cvtColor(img_bgr,cv2.COLOR_BGR2RGB)
    input_tensor = tf.convert_to_tensor(img_rgb)
    input_tensor = input_tensor[tf.newaxis, ...]
    output = model(input_tensor)

    print("num_detections:", output['num_detections'], int(output['num_detections']))

    rate = rospy.Rate(1) #no reason, to use Timers
    while not rospy.is_shutdown():

        #OpenImage
        #Detect trafic border
        #if detected
        pub_detect.publish(True)
        pub_size.publish(Vector3(0,3,0))

        rate.sleep()

if __name__ == '__main__':
    try:
        traffic_light_fetcher()
    except rospy.ROSInterruptException:
        pass
