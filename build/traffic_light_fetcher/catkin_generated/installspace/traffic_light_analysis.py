#!/usr/bin/env python3

import rospy
import threading
import numpy as np
import message_filters
from std_msgs.msg import Bool
from std_msgs.msg import String
from std_msgs.msg import Float32
from geometry_msgs.msg import Vector3

# don't wanna use class
pub_length=rospy.Publisher('traffic_light_length',Float32,queue_size=10)

def traffic_light_analysis():
    rospy.init_node('traffic_light_analysis')
    #not needed, subscriptions are required by the fake task
    s1 = rospy.Subscriber('traffic_light_detected',Bool,traffic_light_detected)
    s2 = rospy.Subscriber('traffic_light_size', Vector3,traffic_light_size    )
    #sync topics
    sub_detect = message_filters.Subscriber('traffic_light_detected',Bool,queue_size=10)
    sub_size   = message_filters.Subscriber('traffic_light_size', Vector3,queue_size=10)
    ts = message_filters.ApproximateTimeSynchronizer([sub_detect,sub_size],10,0.1,allow_headerless=True)
    ts.registerCallback(traffic_light_length)
    rospy.spin()

def traffic_light_length(detected,size):
    if detected:                      #better to check size.x>0 and size.y>0:
        rospy.loginfo(rospy.get_caller_id()+' detected %s, size %s',detected,size)
        pub_length.publish(size.y/3)  #FIXME: deal with horizontal TL

def traffic_light_detected(detected):
    #do nothing, subscription is required by the fake task
    rospy.loginfo(rospy.get_caller_id()+' detected %s',detected)

def traffic_light_size(size):
    ##do nothing, subscription is required by the fake task
    rospy.loginfo(rospy.get_caller_id()+' size: %s',size)

if __name__ == '__main__':
    try:
        traffic_light_analysis()
    except rospy.ROSInterruptException:
        pass
